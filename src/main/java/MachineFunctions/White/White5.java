package MachineFunctions.White;

public class White5 {
    public static String asciiMix(String string1 , String string2){


        char [] a = string1.toCharArray();
        char [] b = string2.toCharArray();
        char [] ab;
        if (string1.length()>string2.length()){
            ab = new char[string1.length()];
            for (int i = 0 ; i < string1.length() ; i++ ){
                int temp1 = a[i];
                if (i>=string2.length()){
                    ab[i]=a[i];
                }else{
                    int temp2 = b[i];
                    int temp3 = ((temp1+temp2-194)%26)+97;
                    ab[i] = (char) temp3;
                }
            }}else {
            ab = new char[string2.length()];
            for (int i = 0 ; i < string2.length() ; i++ ){
                int temp1 = b[i];
                if (i>=string1.length()){
                    ab[i]=b[i];
                }else{
                    int temp2 = a[i];
                    int temp3 = ((temp1+temp2-194)%26)+97;
                    ab[i] = (char) temp3;
                }
            }

        }
        String result = "";
        for (int i = 0 ; i < ab.length ; i++ ){
            result+=ab[i];
        }
        return result;
    }
}
