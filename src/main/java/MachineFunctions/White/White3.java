package MachineFunctions.White;

import MachineFunctions.Black.black1;

public class White3 {
    public static String reverse_mix(String string1,String string2){

        string2 = black1.reverse(string2);
        String result = White1.mix(string1,string2);

        return result;
    }
}
