package MachineFunctions.White;

import BoardColors.Green;
import MachineFunctions.Black.BlackFuncs;

public class WhiteFuncs {
    public static String setWhiteFunctions (int i ,String leftStr , String rightStr){
        switch(i){
            case 1 :
                return White1.mix(leftStr,rightStr);
            case 2:
                return White2.reversePaste(leftStr,rightStr);
            case 3:
                return White3.reverse_mix(leftStr,rightStr);
            case 4:
                return White4.oddOrEven(leftStr,rightStr);
            case 5:
                return White5.asciiMix(leftStr,rightStr);
            default:
                return null;
        }
    }
}


//    public static void main(String []args) {
//
//        String string = "qmiqwnhwnrckeirepjgv";
//        int [][] board = {
//                {2, 5, 5, 4, 2, 1, 5, 5},
//                {2, 1, 2, 4, 4, 1, 5, 4},
//                {4, 4, 1, 1, 1, 5, 1, 4},
//                {4, 1, 4, 4, 1, 4, 5, 1},
//                {1, 1, 1, 5, 1, 4, 4, 5},
//                {4, 4, 5, 4, 5, 1, 5, 5},
//                {1, 4, 4, 4, 1, 1, 1, 4},
//                {4, 5, 4, 5, 5, 1, 4, 4},
//        };
//        String lastRowString = "";
//        String temp;
//
//        for (int i = 0 ; i < 7 ; i++){
//            temp = string;
//            for (int j = 0 ; j < 8 ; j++) {
//                if (j == 7) {
//                    if (i == 0) {
////                        Blue blue = new Blue();
////                        blue.setFuncNum(board[j][i]);
////                        blue.setInput(temp);
////                        temp = blue.doEval();
////                        lastRowString = BlackFuncs.setBlackFunctions(board[j][i],temp);
//                        Green green = new Green();
//                        green.setFuncNum(board[j][i]);
//                        green.setInput(temp);
//                        lastRowString= green.doEval();
//                    } else {
//                        lastRowString = WhiteFuncs.setWhiteFunctions(board[7][i],lastRowString,temp);
//                    }
//                } else {
//                    temp= BlackFuncs.setBlackFunctions(board[j][i],temp);
//                }
//            }
//            string=BlackFuncs.setBlackFunctions(board[0][i],string);
//        }
//        System.out.println(lastRowString);
//    }
//
//}










//
//        String string = "qmiqwnhwnrckeirepjgv";
//        String n = "qmiqwnhwnrckeirepjgv";
//        int [][] board = {
//                {2, 5, 5, 4, 2, 1, 5, 5},
//                {2, 1, 2, 4, 4, 1, 5, 4},
//                {4, 4, 1, 1, 1, 5, 1, 4},
//                {4, 1, 4, 4, 1, 4, 5, 1},
//                {1, 1, 1, 5, 1, 4, 4, 5},
//                {4, 4, 5, 4, 5, 1, 5, 5},
//                {1, 4, 4, 4, 1, 1, 1, 4},
//                {4, 5, 4, 5, 5, 1, 4, 4},
//        };
//
//        String lastRowString = "";
//        String temp;
//        for (int i = 0 ; i < 7 ; i++){
//            temp = string;
//            for (int j = 0 ; j < 8 ; j++) {
//                if (j == 7) {
//                    if (i == 0) {
//                        lastRowString = BlackFuncs.setBlackFunctions(board[j][i],temp);
//                    } else {
//                        lastRowString = WhiteFuncs.setWhiteFunctions(board[7][i],lastRowString,temp);
//                    }
//                } else {
//                    temp=BlackFuncs.setBlackFunctions(board[j][i],temp);
//                }
//            }
//            string=BlackFuncs.setBlackFunctions(board[0][i],string);
//        }
//        return null;
//    }
//}














