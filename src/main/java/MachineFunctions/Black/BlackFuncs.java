package MachineFunctions.Black;

public class BlackFuncs {
    public static String setBlackFunctions(int i, String input){
        switch (i){
            case 1 :
                return( black1.reverse(input));
            case 2 :
                return( black2.repeat(input));
            case 3:
                return( black3.repeat(input));
            case 4 :
                return( black4.shiftChar(input));
            case 5 :
                return( black5.endToFirst(input));
            default:
                return null;
        }
    }
}
