package MachineFunctions.Black;

public class black1 {
    public static String reverse(String string){
        char[] a = string.toCharArray();
        char[] main = new char[string.length()];
        for (int i = string.length()-1 , b=0 ; i >= 0;i--,b++){
            main[b] = a[i];
        }
        String result ="";
        for (int i =  0 ; i<string.length();i++){
            result+=main[i];
        }
        return result;
    }
}
