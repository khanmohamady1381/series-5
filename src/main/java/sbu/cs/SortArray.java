package sbu.cs;

import static java.util.Arrays.sort;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0; i < size-1; i++)
        {
            int min_idx = i;
            for (int j = i+1; j < size; j++)
                if (arr[j] < arr[min_idx])
                    min_idx = j;
            int temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i=1; i<size; ++i)
        {
            int key = arr[i];
            int j = i-1;
            while (j>=0 && arr[j] > key)
            {
                arr[j+1] = arr[j];
                j = j-1;
            }
            arr[j+1] = key;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        Sort(arr,0,size-1);

        return arr;
    }
    public static void merge(int[] a,int l,int m,int h)
    {
        int i, j,c=l;
        int[] b=new int[h+1];

        for(i = l,j = m+1; i<=m && j<=h; c++)
        {

            if(a[i] <= a[j])
                b[c] = a[i++];
            else
                b[c] = a[j++];
        }
        while(i <= m )
            b[c++] = a[i++];

        while(j<=h)
            b[c++] = a[j++];

        for(i = l ; i <= h; i++)
            a[i] = b[i];
    }

    public static void Sort(int [] a,int l,int h)
    {
        if(l<h)
        {
            int m=(l+h)/2;
            Sort(a,l,m);
            Sort(a,m+1,h);
            merge(a,l,m,h);

        }


    }


    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int first = 0;
        int last = arr.length;
        int mid  = (first + last)/2;
        while(first<=last){

            if (arr[mid]<value)
            {
                first = mid + 1 ;
            }else if(arr[mid]==value){
                return mid;
            }else {
                last = mid - 1 ;
            }
            mid = (first+last)/2;
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        int first = 0 ;
        int last = arr.length-1;
        return rec_bin_search(arr,first, last,value);
    }

   public int rec_bin_search(int [] arr, int first, int last, int x){
        if (last >= first){
            int mid = first + (last - first) / 2;
            if (arr[mid] == x)
                return mid;
            if (arr[mid] > x)
                return rec_bin_search(arr, first, mid - 1, x);
            return rec_bin_search(arr, mid + 1, last, x);
        }
        return -1;
        }
   }
