package sbu.cs;

import BoardColors.Blue;
import BoardColors.Green;
import BoardColors.Pink;
import BoardColors.Yellow;

public class App {
    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {

        Pink pink = new Pink();

        pink.setFuncNum(arr[n-1][n-1]);
        pink.setLeftStr(coloumnEvluation(arr,input));
        pink.setRightStr(rowEvaluation(arr,input));

        return pink.doEval();

    }

    public static String rowEvaluation(int [][] board , String input){

        Green green = new Green();
        Pink pink = new Pink();
        Blue blue = new Blue();
        Yellow yellow = new Yellow();

        String lastRowString = "";
        String temp;

        for (int j= 0 ; j < 7 ; j++){
            temp = input;
            for (int i = 0 ; i < 8 ; i++) {
                if (i == 7) {
                    if (j == 0) {
                        yellow.setFuncNum(board[j][i]);
                        yellow.setInput(temp);
                        lastRowString = yellow.doEval();
                    } else {
                        pink.setLeftStr(lastRowString);
                        pink.setRightStr(temp);
                        pink.setFuncNum(board[7][i]);
                        lastRowString = pink.doEval();
                    }
                } else {
                    blue.setInput(temp);
                    blue.setFuncNum(board[j][i]);
                    temp = blue.doEval();
                }
            }
            green.setInput(input);
            green.setFuncNum(board[0][j]);
            input = green.doEval();
        }
        return lastRowString;
    }

    public static String coloumnEvluation(int [][] board , String input){

        Green green = new Green();
        Pink pink = new Pink();
        Blue blue = new Blue();
        Yellow yellow = new Yellow();

        String lastColumnString = "";
        String temp;

        for (int i= 0 ; i < 7 ; i++){
            temp = input;
            for (int j = 0 ; j < 8 ; j++) {
                if (j == 7) {
                    if (i == 0) {
                        yellow.setFuncNum(board[j][i]);
                        yellow.setInput(temp);
                        lastColumnString = yellow.doEval();
                    } else {
                        pink.setLeftStr(lastColumnString);
                        pink.setRightStr(temp);
                        pink.setFuncNum(board[7][i]);
                        lastColumnString = pink.doEval();
                    }
                } else {
                    blue.setInput(temp);
                    blue.setFuncNum(board[j][i]);
                    temp = blue.doEval();
                }
            }
            green.setInput(input);
            green.setFuncNum(board[0][i]);
            input = green.doEval();
        }
        return lastColumnString;
    }

}

