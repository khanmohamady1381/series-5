package BoardColors;

import MachineFunctions.Black.BlackFuncs;

public class Blue {
    private String input ;
    private int funcNum ;

    public void setInput(String input) {
        this.input = input;
    }

    public void setFuncNum(int funcNum) {
        this.funcNum = funcNum;
    }

    public String doEval(){
        return BlackFuncs.setBlackFunctions(funcNum,input);
    }
}
