package BoardColors;

import MachineFunctions.White.WhiteFuncs;

public class Pink {
    private String leftStr;
    private String rightStr;
    private int funcNum;

    public void setLeftStr(String leftStr) {
        this.leftStr = leftStr;
    }

    public void setRightStr(String rightStr) {
        this.rightStr = rightStr;
    }

    public void setFuncNum(int funcNum) {
        this.funcNum = funcNum;
    }

    public String doEval(){
        return WhiteFuncs.setWhiteFunctions(funcNum,leftStr,rightStr);
    }


}
